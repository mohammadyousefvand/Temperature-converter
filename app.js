let $ = document

let converter = $.getElementById('converter')
let convertButton = $.querySelector('.convertButton')
let resetButton = $.querySelector('.resetButton')
let changeButton = $.querySelector('.changeButton')

let result = $.querySelector('.result')
let firstValue = $.querySelector('.C')
let secondValue = $.querySelector('.F')


function convert() {
    if (converter.value === '') {
        result.innerHTML = 'Enter the correct value'
        result.style.color = 'red'
    } else if (isNaN(converter.value)) {
        result.innerHTML = 'Wrong value!'
        result.style.color = 'red'
    } else {
        if (firstValue.innerHTML === '°C') {
            let resultValue = (converter.value * 9 / 5) + 32
            result.innerHTML = resultValue + ' °F'
            result.style.color = 'green'
        } else {
            let resultValue = (converter.value - 32) * 5 / 9
            result.innerHTML = resultValue + ' °C'
            result.style.color = 'green'
        }
    }
}

function reset() {
    result.innerHTML = ''
    converter.value = ''
}

function change() {

    if (firstValue.innerHTML === '°C') {
        firstValue.innerHTML = '°F'
        secondValue.innerHTML = '°C'
        document.title = 'F to C convertor'
        converter.setAttribute('placeholder', '°F')

    } else {
        firstValue.innerHTML = '°C'
        secondValue.innerHTML = '°F'
        document.title = 'C to F convertor'
        converter.setAttribute('placeholder', '°C')

    }
}

convertButton.addEventListener('click', convert)

resetButton.addEventListener('click', reset)

changeButton.addEventListener('click', change)